# WildCount :: Gap8 platform

* SDK Manuals https://greenwaves-technologies.com/sdk-manuals/
* SDK https://github.com/GreenWaves-Technologies/gap_sdk
* Developer Forum https://greenwaves-technologies.com/gap8_developers_forum-2/ 



## Hardware requirement
* Boards
    * [GAPPoc-A , Computer Vision Concept Board](https://greenwaves-technologies.com/product/gappoc-a-computer-vision-concept-board/)
    * [GAPPoc-B, Occupancy Management Reference Platform](https://greenwaves-technologies.com/product/gappoc-b-occupancy-management-reference-platform/) with [Lynred thermal sensor](https://lynred.com/product/thermeye-b90-b120)
* JTAG
    * [ARM-USB-OCD](https://www.olimex.com/Products/ARM/JTAG/ARM-USB-OCD/)
    * [J-Link EDU](https://www.segger.com/products/debug-probes/j-link/models/j-link-edu/)
    * [Sipeed USB-JTAG/TTL RISC-V Debugger](https://www.seeedstudio.com/Sipeed-USB-JTAG-TTL-RISC-V-Debugger-p-2910.html)
* Adaptateur
    * [ARM-JTAG-20-10](https://www.olimex.com/Products/ARM/JTAG/ARM-JTAG-20-10/)
* Energy monitoring and Power supply
    * [X-NUCLEO-LPM01A](https://www.st.com/en/evaluation-tools/x-nucleo-lpm01a.html)

## Software requirement
* Energy monitoring
    * [STM32CubeMonitor-Power (STM32CubeMonPwr)](https://www.st.com/content/st_com/en/products/development-tools/software-development-tools/stm32-software-development-tools/stm32-performance-and-debuggers/stm32cubemonpwr.html)


## Getting started with [GAPPoc-A , Computer Vision Concept Board](https://greenwaves-technologies.com/product/gappoc-a-computer-vision-concept-board/)


Follow the instruction of https://github.com/GreenWaves-Technologies/gap_sdk

TODO


## GVSOC Virtual Platform
* https://greenwaves-technologies.com/manuals/BUILD/GVSOC/html/index.html
* https://greenwaves-technologies.com/gvsoc-the-full-system-simulator-for-profiling-gap-applications/


## Energy monitoring and Power supply

Use the [X-NUCLEO-LPM01A](https://www.st.com/en/evaluation-tools/x-nucleo-lpm01a.html) board as power supply. You should first remove the 2/3AA battery and you should wire the pin 4 of CONN4 to 3.3V source and the pin 7 of CONN4 to GND source.

You can monitor the energy consumption on the X-NUCLEO-LPM01A's LCD screen or by using the [STM32CubeMonitor-Power (STM32CubeMonPwr)](https://www.st.com/content/st_com/en/products/development-tools/software-development-tools/stm32-software-development-tools/stm32-performance-and-debuggers/stm32cubemonpwr.html) tool.